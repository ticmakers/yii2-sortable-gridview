<?php

namespace ticmakers\sortablegridview;

use kartik\dynagrid\DynaGrid;
use kartik\dynagrid\DynaGridAsset;
use yii\helpers\Json;

class SortableGridView extends DynaGrid
{

    /**
     * (required) The URL of related SortableAction
     *
     * @see \richardfan1126\sortable\SortableAction
     * @var string
     */
    public $sortUrl;
    /**
     * (optional) The text shown in the model while the server is reordering model
     * You can use HTML tag in this attribute.
     *
     * @var string
     */
    public $sortingPromptText = 'Loading...';
    /**
     * (optional) The text shown in alert box when sorting failed.
     *
     * @var string
     */
    public $failText = 'Fail to sort';

    public $showPersonalize = false;
    public function init()
    {
        parent::init();
        if (!isset($this->sortUrl)) {
            throw new InvalidConfigException("You must specify the sortUrl");
        }
        DynaGridAsset::register($this->view);
        SortableGridViewAsset::register($this->view);

        $this->gridOptions['tableOptions']['class'] = ' sortable-grid-view';
    }

    public function run()
    {
        $this->id = $this->options['id'];
        parent::run();
        $options = [
            'id' => $this->id,
            'action' => $this->sortUrl,
            'sortingPromptText' => $this->sortingPromptText,
            'sortingFailText' => $this->failText,
            'csrfTokenName' => \Yii::$app->request->csrfParam,
            'csrfToken' => \Yii::$app->request->csrfToken,
        ];
        $options = Json::encode($options);
        $js = <<<JS
        try {
            jQuery.SortableGridView($options);
        } catch (err) {
            console.error(err)
        }
JS;
        $this->view->registerJs($js);
    }
}
